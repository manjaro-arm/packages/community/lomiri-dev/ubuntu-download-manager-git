# Maintainer: Ivan Semkin (ivan at semkin dot ru)

pkgname=lomiri-download-manager-git
_pkgname=lomiri-download-manager
pkgver=r2584.0.1.1
pkgrel=1
pkgdesc='Provides a service for downloading files while an application is suspended'
url='https://gitlab.com/ubports/development/core/lomiri-download-manager'
arch=(x86_64 i686 armv7h aarch64)
license=(LGPL)
conflicts=(ubuntu-download-manager lomiri-download-manager)
provides=(lomiri-download-manager)
depends=(boost dbus-test-runner gmock qt5-base qt5-declarative libnih google-glog clang)
makedepends=(git doxygen cmake cmake-extras-git gcovr lcov)
source=('git+https://gitlab.com/ubports/development/core/lomiri-download-manager.git#branch=main'
        'QtArg.patch'
        'wno-error.patch')
sha256sums=('SKIP'
            '5c025509bf51586b39fc8881037eb6b9bce173421e8ca6d2f0eae19c350164c7'
            'f8f952c81a94257887d4578870c77c249661dbe7ef4827fd8d32b28b9ea21c05')

BUILDENV+=('!check') # Some tests are broken

prepare() {
  cd ${_pkgname}
  patch -Np1 -i "${srcdir}/QtArg.patch"
  patch -Np1 -i "${srcdir}/wno-error.patch"
}

pkgver() {
  cd ${_pkgname}
  echo "r$(git rev-list --count HEAD).$(git describe --always)"
}

build() {
  cd ${_pkgname}
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_INSTALL_LIBDIR="lib/" -DCMAKE_INSTALL_LIBEXECDIR="lib/" .
  make
}

check() {
  cd ${_pkgname}
  make ARGS+="--output-on-failure" test
}

package() {
  cd ${_pkgname}
  make DESTDIR="${pkgdir}/" install
  mv ${pkgdir}/usr/lib/qt5/ ${pkgdir}/usr/lib/qt/
}
